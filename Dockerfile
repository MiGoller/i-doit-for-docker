# Use phusion/baseimage as base image. To make your builds reproducible, make
# sure you lock down to a specific version, not to `latest`!
# See https://github.com/phusion/baseimage-docker/blob/master/Changelog.md for
# a list of version numbers.

FROM phusion/baseimage:0.11

# Build arguments ...
# Version information
ARG ARG_APP_VERSION=1.14.1
ARG ARG_IMAGE_VERSION_BUILD=0

# The channel or branch triggering the build.
ARG ARG_APP_CHANNEL

# The commit sha triggering the build.
ARG ARG_APP_COMMIT

# Build data
ARG ARG_BUILD_DATE

# Code branch, defaults to master
ARG ARG_APP_BRANCH

# App flavor
ARG ARG_FLAVOR

# PHP version
ARG ARG_PHP_VERSION=7.2

# Basic build-time metadata as defined at http://label-schema.org
LABEL org.label-schema.build-date=${ARG_BUILD_DATE} \
    org.label-schema.docker.dockerfile="/Dockerfile" \
    org.label-schema.license="GPLv3" \
    org.label-schema.name="MiGoller" \
    org.label-schema.vendor="MiGoller" \
    org.label-schema.version="${ARG_APP_VERSION}.${ARG_IMAGE_VERSION_BUILD}-${ARG_APP_BRANCH}" \
    org.label-schema.description="i-doit" \
    org.label-schema.url="https://gitlab.com/users/MiGoller/projects" \
    org.label-schema.vcs-ref=${ARG_APP_COMMIT} \
    org.label-schema.vcs-type="Git" \
    org.label-schema.vcs-url="https://gitlab.com/MiGoller/i-doit-for-docker.git" \
    maintainer="MiGoller" \
    Author="MiGoller"

# Persist app-reladted build arguments and set default values for container environment variables
ENV APP_VERSION=$ARG_APP_VERSION.$ARG_IMAGE_VERSION_BUILD \
    APP_CHANNEL=$ARG_APP_CHANNEL \
    APP_COMMIT=$ARG_APP_COMMIT \
    APP_FLAVOR=${ARG_FLAVOR} \
    APP_BRANCH=${ARG_APP_BRANCH} \
	MYSQL_HOST= \
	MYSQL_PORT=3306 \
	MYSQL_ROOT_USER=root \
	MYSQL_ROOT_PASSWORD= \
	MANDATOR_NAME="Your Company" \
	SYSTEM_DB_NAME=idoit_system \
	MANDATOR_DB_NAME=idoit_data \
	ADMIN_CENTER_PASSWORD=

# Use baseimage-docker's init system.
CMD ["/sbin/my_init"]

# ...put your own build instructions here...
RUN apt update \
	&& apt-get install -y \
	apache2 libapache2-mod-php${ARG_PHP_VERSION} \
	mariadb-client \
	php${ARG_PHP_VERSION}-bcmath php${ARG_PHP_VERSION}-cli php${ARG_PHP_VERSION}-common php${ARG_PHP_VERSION}-curl php${ARG_PHP_VERSION}-gd php${ARG_PHP_VERSION}-json \
	php${ARG_PHP_VERSION}-ldap php${ARG_PHP_VERSION}-mbstring php${ARG_PHP_VERSION}-mysql php${ARG_PHP_VERSION}-opcache php${ARG_PHP_VERSION}-pgsql \
	php${ARG_PHP_VERSION}-soap php${ARG_PHP_VERSION}-xml php${ARG_PHP_VERSION}-zip \
	php-imagick php-memcached \
	memcached unzip moreutils wget

# download i-doit and unpack it to the i-doit homedir
RUN mkdir /var/www/html/i-doit \
	&& wget -O i-doit.zip https://sourceforge.net/projects/i-doit/files/i-doit/${ARG_APP_VERSION}/idoit-open-${ARG_APP_VERSION}.zip/download \
	&& unzip i-doit.zip -d /var/www/html/i-doit \
	&& rm i-doit.zip \
	&& cd /var/www/html/i-doit/ \ 
	&& chown www-data:www-data -R . \ 
	&& find . -type d -name \* -exec chmod 775 {} \; \ 
	&& find . -type f -exec chmod 664 {} \; \ 
	&& chmod 774 controller *.sh setup/*.sh

# Copy configuration files
ADD etc/apache2/sites-available /etc/apache2/sites-available
ADD etc/php/7.2/mods-available /etc/php/${ARG_PHP_VERSION}/mods-available

# Final modifications
RUN phpenmod i-doit \
	&& phpenmod memcached \
	&& a2dissite 000-default \
	&& chown www-data:www-data -R /var/www/html/ \ 
	&& chmod 755 /var/log/apache2 \ 
	&& chmod 664 /var/log/apache2/* \ 
	&& a2ensite i-doit \ 
	&& a2enmod rewrite

# Clean up APT when done.
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Copy phusion startup scripts
RUN mkdir -p /etc/my_init.d
ADD etc/service /etc/service
ADD scripts/my_init.d /etc/my_init.d
RUN chmod +x /etc/my_init.d/*.sh \
	&& chmod +x /etc/service/apache/run \
	&& chmod +x /etc/service/memcached/run
