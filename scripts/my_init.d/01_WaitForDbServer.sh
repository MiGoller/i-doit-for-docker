#!/bin/bash

set -e

echo "========================================================================="
echo "-------------------------------------------------------------------------"
echo "        Wait for MariaDB server ${MYSQL_HOST} ..."
echo "-------------------------------------------------------------------------"

# Waiting for connection to MariaDB server ...
if [ -n "${MYSQL_HOST}" ]; then
    echo -n "Waiting for connection to MariaDB server on $MYSQL_HOST: "
    while ! mysqladmin ping -h"$MYSQL_HOST" -s; do
        sleep 1
        echo -n "."
    done
    echo " established."
fi

echo "========================================================================="
