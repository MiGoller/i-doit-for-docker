#!/bin/bash

set -e

echo "========================================================================="
echo "-------------------------------------------------------------------------"
echo "        Checking for first run ..."
echo "-------------------------------------------------------------------------"
FILENAME="/var/www/html/i-doit/src/config.inc.php"
FIRSTRUN=""
 
if [ -f ${FILENAME} ]; then
    if [ -s ${FILENAME} ]; then
        echo "i-doit configuration file ${FILENAME} exists. Aborting first run."
        FIRSTRUN=""
    else
        echo "i-doit configuration file ${FILENAME} exists but empty."
        FIRSTRUN="y"
    fi
else
    echo "i-doit configuration file ${FILENAME} does not exists."
    FIRSTRUN="y"
fi

if [  "${FIRSTRUN}" == "y" ]; then
    # i-doit command line installer

    # Usage: $0 options
    # Example: $0 -m idoit_data_install -s idoit_system_install -n "install-test"

    # OPTIONS:
    #    -d  db_suffix
    #    -n  mandator name
    #    -s  system database name
    #    -m  mandator database name
    #    -h  mysql host (default: localhost)
    #    -u  mysql user (default: root)
    #    -p  mysql root password
    #    -P  mysql port (default: 3306)
    #    -a  set admin center password (default: empty)
    #    -r  revert (uninstall)
    #    -U  help
    #    -q  quiet mode

    echo "Running automated i-doit installer. Please wait ..."
    cd /var/www/html/i-doit/setup

    bash ./install.sh \
        -n "${MANDATOR_NAME}" \
        -s ${SYSTEM_DB_NAME} -m ${MANDATOR_DB_NAME} \
        -h ${MYSQL_HOST} -P ${MYSQL_PORT} -u ${MYSQL_ROOT_USER} -p "${MYSQL_ROOT_PASSWORD}" \
        -a "${ADMIN_CENTER_PASSWORD}" \
        -q
    
    echo "i-doit installer finished."
fi

echo "========================================================================="
