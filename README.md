#   Supported tags and respective `Dockerfile` links

-   `1.14.1.0`, `1.14`, `1`, `latest` [Dockerfile](https://gitlab.com/MiGoller/i-doit-for-docker/-/blob/771e99f70487dce91ed96de2e434b721908c6f20/Dockerfile)
-   `1.14.0` [Dockerfile](https://gitlab.com/MiGoller/i-doit-for-docker/-/blob/2dee9408467cb98e126aa495bf4d81c2a0a6bdec/Dockerfile)

#   What is i-doit? 

> The IT-documentation solution i-doit is based on a complete open source configuration management and database. Using i-doit as a CMDB you can manage your IT according to ITIL best practices and configurate the significant components of your IT environment. You can visualize the relationships between these components and keep track of software and hardware assets and their relations.

https://www.i-doit.org/

#   How to use this image

`i-doit for Docker` is based on the [phusion baseimage for Docker](https://github.com/phusion/baseimage-docker) to provide a stable and reliable Ubuntu platform for [i-doit](https://www.i-doit.org/).

This image does not provide a MariaDB or MySQL database server for hosting the [i-doit](https://www.i-doit.org/) databases. Feel free to store the databases to your existing MariaDB or MySQL server or to add a MariaDB server to your i-doit stack.

##  Start an `i-doit for Docker` instance

Starting an `i-doit for Docker` instance is simple:

```
$ docker run -d --name my-i-doit \
    -p 80:80
    -v ./example/conf/config.inc.php:/var/www/html/i-doit/src/config.inc.php
    migoller/idoit:tag
```

Where `my-i-doit` is the name you want to assign to your container, and `tag` is the tag specifying the i-doit version you want. See the list above for relevant tags.

The container runs `i-doit for Docker` on port 80. You'll have to expose that port to the outer world until you run `i-doit for Docker` behind a reverse proxy like Traefik, nginx, Apache, etc.


`i-doit for Docker` will store the runtime configuration to a file called `config.inc.php`. Add a volume mount for that file to persist the configuration outside the container!

##  Start an `i-doit for Docker` stack

The `i-doit for Docker` image does not provide any database management system. It's up to you to create a new database management system like MariaDB or to connect to an already existing DBMS.

But it's easy to run a complete `i-doit for Docker` stack using `docker-compose`. Have a look at the repository for further details. Check the folder `example` for additional configuration files to automate the stack.

A typical `docker-compose.yml` file looks similar to this:

```
version: '2'
services:
  app:
    # image: migoller/idoit
    build: .
    volumes:
      - ./example/conf/config.inc.php:/var/www/html/i-doit/src/config.inc.php
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro
    ports:
      - "8080:80"
    networks:
      - default
    env_file:
      - ./example/idoit.env

  db:
    image: mariadb
    ports:
      - 3306:3306
    networks:
      - default
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - /etc/timezone:/etc/timezone:ro
      - /storage/local/test/i-doit/mariadb/data:/var/lib/mysql
      - ./example/conf/99-i-doit.cnf:/etc/mysql/mariadb.conf.d/99-i-doit.cnf
    env_file:
      - ./example/idoit.env
```

The `i-doit for Docker` stack will run two containers (services) sharing the same environments variables for automated setup and run-time access.

- app: The `i-doit for Docker` container.
- db: The MariaDB database server for `i-doit for Docker`.

The environment variables are stored in a file `idoit.env`, so you have to enter credentials, etc. only once. I recommend to read about Docker secrets for further details regarding security.

```
MYSQL_HOST=db
MYSQL_PORT=3306
MYSQL_ROOT_USER=root
MYSQL_ROOT_PASSWORD=9JQEJyxu0Zd81E8dLkqD
MANDATOR_NAME="i-doit on Docker"
SYSTEM_DB_NAME=idoit_system
MANDATOR_DB_NAME=idoit_data
ADMIN_CENTER_PASSWORD=YourVerySecretPassword
```

Set the variable `MYSQL_HOST` to the service name of the MariaDB service (`db`).

##  Configuring `i-doit for Docker`

The best way to provide configuration to the `i-doit for Docker` image is to provide a custom `env` file and a `config.inc.php` file to `i-doit for Docker`. 

For migration scenarios feel free to mount an already existing `config.inc.php` file to `i-doit for Docker` container. Check the existing configuration file for needed modifications regarding filesystem or networking settings.

### Automated installer

The automated installer will setup `i-doit for Docker` when the provided `config.inc.php` file is empty or missing.

You'll have to pass the full set of environment variables to the `i-doit for Docker` container on first run to enable the automated installer to set up your on-premises `i-doit for Docker` installation. The installer will store these information to the `config.inc.php` file. 

Don't forget to create a volume mount for this file to persist the configuration data!

### i-doit common configuration file

`i-doit` stores the run-time configuration to the `config.inc.php` file. Don't forget to create a volume mount for this file to persist the configuration data!

Keep in mind that run-time reconfiguration of `i-doit for Docker` will overwrite the `config.inc.php` file.

### Environment variables

In addition, a few environment variables are provided to set very common parameters:

- `MYSQL_HOST` and `MYSQL_PORT` will define the MariaDB server. Set it accordingly.
- `MYSQL_ROOT_USER` and `MYSQL_ROOT_PASSWORD` will enable the installer to setup at least two databases for i-doit. i-doit will create a new sql user account to access the data during run-time.
- `MANDATOR_NAME` sets the name for the first i-doit tenant. Set to your organization name, etc.
- `SYSTEM_DB_NAME` and `MANDATOR_DB_NAME` set the names for the two databases the installer will create.
- `ADMIN_CENTER_PASSWORD` sets the password for i-doit's administrator UI.

## Caveats
### Where to Store Data

Important note: There are several ways to store data used by applications that run in Docker containers. I encourage users of the `i-doit for Docker` images to familiarize themselves with the options available, including:

- Let Docker manage the storage of your data by writing the files to disk on the host system using its own internal volume management. This is the default and is easy and fairly transparent to the user. The downside is that the files may be hard to locate for tools and applications that run directly on the host system, i.e. outside containers.

- Create a data directory on the host system (outside the container) and mount this to a directory visible from inside the container. This places the database files in a known location on the host system, and makes it easy for tools and applications on the host system to access the files. The downside is that the user needs to make sure that the directory exists, and that e.g. directory permissions and other security mechanisms on the host system are set up correctly.

The Docker documentation is a good starting point for understanding the different storage options and variations, and there are multiple blogs and forum postings that discuss and give advice in this area.

#   MIT License

Copyright (c) 2020 MiGoller

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
