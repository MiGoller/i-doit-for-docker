#!/bin/bash

set -e

if [ -z ${1+x} ]; then
    echo "Usage: ./buildImage.sh [branch1, branch2, ...]"
    exit 1
else
    branches=( ${1//,/ } )

    export BUILD_DATE=$(date +"%Y.%m.%d")

    for branch in "${branches[@]}"; do
        # # Get version information for branch
        # echo "  - Get version information for branch ${branch}..."
        # rm -f ./ioBroker_version.json
        # wget -q https://raw.githubusercontent.com/ioBroker/ioBroker/${branch}/package.json -O ./ioBroker_version.json
        
        # # Get the current version information for the app
        # export APP_VERSION=$( node -pe "require('./ioBroker_version.json')['version']" )
        # echo "  - Setting APP_VERSION to ${APP_VERSION} ..."

        export BUILD_IMAGE_NAME="${CONTAINER_TEST_IMAGE}-${branch,,}"

        echo "  - Building image: ${BUILD_IMAGE_NAME} ..."
        docker build --pull -f ./Dockerfile -t ${BUILD_IMAGE_NAME} \
            --build-arg 1ARG_APP_VERSION=$APP_VERSION \
            --build-arg ARG_APP_CHANNEL=$CI_COMMIT_REF_SLUG \
            --build-arg ARG_APP_COMMIT=$CI_COMMIT_SHA \
            --build-arg ARG_BUILD_DATE="$BUILD_DATE" \
            --build-arg ARG_FLAVOR="" \
            --build-arg ARG_APP_BRANCH="${branch}" .

        echo "  - Pushing image to GitLab repository: ${BUILD_IMAGE_NAME} ..."
        if [ "${FAKE_CI}" = "true" ]; then
            echo "    fake --> docker push ${BUILD_IMAGE_NAME} ..."
        else
            docker push ${BUILD_IMAGE_NAME}
        fi
    done
fi
