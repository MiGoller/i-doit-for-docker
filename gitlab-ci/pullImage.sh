#!/bin/bash

set -e

if [ -z ${1+x} ]; then
    echo "Usage: ./pullImage.sh [branch1, branch2, ...]"
    exit 1
else
    branches=( ${1//,/ } )

    for branch in "${branches[@]}"; do
        # Pull image for tagging, etc.
        export BUILD_IMAGE_NAME="${CONTAINER_TEST_IMAGE}-${branch,,}"

        echo "  - Pulling ${BUILD_IMAGE_NAME} ..."
        if [ "${FAKE_CI}" = "true" ]; then
            echo "    fake --> docker pull ${BUILD_IMAGE_NAME} ..."
        else
            docker pull ${BUILD_IMAGE_NAME}
        fi
    done
fi
