#!/bin/bash

set -e

#   ./releaseImage.sh "$SOURCE_IMAGE_NAME" "$DEST_IMAGE_NAME" "$APP_FLAVOR" "$APP_VERSION" "$APP_SPECIAL_VERSIONS" "$APP_ISDEFAULT" "$APP_SPECIAL_TAGS" "$APP_SUFFIX" "$APP_DISTRO" "lockedTag1, lockedTag2, ..."

###############################################################################
#   Mandatory commandline arguments:
#       [branch1, branch2, ...] :   List of branches to process
#       DEST_IMAGE_NAME         :   The name of the destination image WITHOUT ANY TAG
#       APP_FLAVOR              :   Set to tag images for different variants like "official", "iot", whatever you want.
#       FORCE_APP_VERSION       :   Set to the application's version information
#       APP_SPECIAL_VERSIONS    :   Add these comma-seperated version information to the list of app's version based tag list
#       APP_ISDEFAULT           :   Set to true, if you want to include default tags like the version information only.
#       APP_SPECIAL_TAGS        :   Add these comma-seperated tags like "latest","stable"
#       APP_SUFFIX              :   Add a suffix to each APP_FLAVOR based tag, but not to the default tags.
#       APP_DISTRO              :   Subfolder containing the Dockerfile
#       [lockedTags list]       :   Comma-seperated list of locked tags
###############################################################################

if [ -z ${1+x} ]; then
    echo "Usage: ./releaseImage.sh [branch1, branch2, ...] [DEST_IMAGE_NAME] [APP_FLAVOR] [FORCE_APP_VERSION] [APP_SPECIAL_VERSIONS] [APP_ISDEFAULT] [APP_SPECIAL_TAGS] [APP_SUFFIX] [APP_DISTRO] [lockedTag1, lockedTag2, ...]"
    exit 1
else
    branches=( ${1//,/ } )
    DEST_IMAGE_NAME=${2}
    APP_FLAVOR=${3}
    FORCE_APP_VERSION=${4}
    APP_SPECIAL_VERSIONS=${5,,}
    APP_ISDEFAULT=${6}
    APP_SPECIAL_TAGS=${7}
    MAIN_APP_SUFFIX=${8}
    APP_DISTRO=${9}
    APP_LOCKED_TAGS=${10,,}

    for branch in "${branches[@]}"; do
        # For each branch ...
        echo "Releasing image(s) for branch ${branch}:"

        export BUILD_IMAGE_NAME="${CONTAINER_TEST_IMAGE}-${branch,,}"

        # Releasing image ...
        echo "  - Releasing ${BUILD_IMAGE_NAME} ..."

        # Set version information
        case "${FORCE_APP_VERSION}" in
            "-")
                # NO VERSION TAGGING!
                export APP_VERSION="-"
                ;;
            
            image)
                # Get Version information from image
                export APP_VERSION=$( docker inspect -f "{{range .Config.Env}}{{println .}}{{end}}" ${BUILD_IMAGE_NAME} | grep APP_VERSION | awk '{split($0,a,"="); print a[2]}' )
                ;;
            
            "-image")
                # Get Version information from image
                export APP_VERSION=-$( docker inspect -f "{{range .Config.Env}}{{println .}}{{end}}" ${BUILD_IMAGE_NAME} | grep APP_VERSION | awk '{split($0,a,"="); print a[2]}' )
                ;;

            *)
                # Set to given version information
                export APP_VERSION=${FORCE_APP_VERSION}
        esac

        echo "    - Version $APP_VERSION ..."
        
        # Set Docker image build arguments
        export APP_FLAVOR="${image}"
        export APP_DISTRO="${image}"

        if [ "${branch}" = "master" ] || [ "${branch,,}" = "release" ]; then
            export APP_SUFFIX="${MAIN_APP_SUFFIX}"
        else
            if [ -n "${MAIN_APP_SUFFIX}" ]; then
                export APP_SUFFIX="${MAIN_APP_SUFFIX}-${branch}"
            else
                export APP_SUFFIX="${branch}"
            fi
        fi

        # Tag and push the image
        if [ "${FAKE_CI}" = "true" ]; then
            # ./createTagList.sh "fpm" "15.0.7" "" "false" "" "-smb" "fpm" "fpm-smb"
            echo "./createTagList.sh \"${APP_FLAVOR,,}\" \"$APP_VERSION\" \"$APP_SPECIAL_VERSIONS\" \"$APP_ISDEFAULT\" \"$APP_SPECIAL_TAGS\" \"${APP_SUFFIX,,}\" \"$APP_DISTRO\" \"${APP_LOCKED_TAGS,,}\""
            $( ./createTagList.sh "${APP_FLAVOR,,}" "$APP_VERSION" "$APP_SPECIAL_VERSIONS" "$APP_ISDEFAULT" "$APP_SPECIAL_TAGS" "${APP_SUFFIX,,}" "$APP_DISTRO" "${APP_LOCKED_TAGS,,}")
            echo "    - Image name: ${DEST_IMAGE_NAME}:[${TAG_LIST}]"
        else
            # echo "    - 1"
            # ./releaseDockerImage.sh "$SOURCE_IMAGE_NAME" "$DEST_IMAGE_NAME" "$APP_FLAVOR" "$APP_VERSION" "$APP_SPECIAL_VERSIONS" "$APP_ISDEFAULT" "$APP_SPECIAL_TAGS" "$APP_SUFFIX" "$APP_DISTRO"
            ./releaseDockerImage.sh \
                "${BUILD_IMAGE_NAME}" "${DEST_IMAGE_NAME}" \
                "${APP_FLAVOR,,}" "$APP_VERSION" "$APP_SPECIAL_VERSIONS" "$APP_ISDEFAULT" "$APP_SPECIAL_TAGS" "${APP_SUFFIX,,}" "$APP_DISTRO" "${APP_LOCKED_TAGS,,}"
        fi
    done
fi
