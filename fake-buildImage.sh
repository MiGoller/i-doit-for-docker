#!/bin/bash

set -e

export CONTAINER_TEST_IMAGE=idoitstage
export FAKE_CI=true

/bin/bash ./gitlab-ci/buildImage.sh "develop"
