#!/bin/bash

set -e

export CONTAINER_TEST_IMAGE=idoitstage
export REGISTRY_IMAGE=migoller/idoit
export FAKE_CI=true

# ./releaseImage.sh [branch1, branch2, ...] [DEST_IMAGE_NAME] \
#   [APP_FLAVOR] [FORCE_APP_VERSION] [APP_SPECIAL_VERSIONS] [APP_ISDEFAULT] [APP_SPECIAL_TAGS] [APP_SUFFIX] [APP_DISTRO] [lockedTag1, lockedTag2, ...]


echo "----  MASTER-images  ----"
/bin/bash ./gitlab-ci/releaseImage.sh \
    "develop" "testimagename" \
    "" "-image" "latest" "false" "" "" "" ""

/bin/bash ./gitlab-ci/releaseImage.sh \
    "develop" "testimagename" \
    "" "image" "" "false" "latest" "" "" ""
